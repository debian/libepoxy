libepoxy (1.5.3-1) UNRELEASED; urgency=medium

  * rules: Also ignore test failure on hurd-amd64.

 -- Samuel Thibault <sthibault@debian.org>  Sun, 12 Nov 2023 19:03:59 +0100

libepoxy (1.5.3-0.1) unstable; urgency=medium

  * Non-maintainer upload
  * New upstream release
  * debian/copyright: Stop excluding .gitignore
  * Drop workaround specifying Xvfb screen size since #874077 is fixed
  * Bump minimum meson to 0.47.0
  * Drop patch: applied in new release
  * Drop obsolete shlib-calls-exit lintian override
  * Enable all hardening flags

 -- Jeremy Bicha <jbicha@debian.org>  Thu, 04 Oct 2018 20:28:06 -0400

libepoxy (1.5.2-0.3) unstable; urgency=medium

  * Non-maintainer upload
  * Drop obsolete python:native build dependency
  * Don't ignore test failures except on s390x and on ports where tests fail

 -- Jeremy Bicha <jbicha@debian.org>  Wed, 19 Sep 2018 22:18:18 -0400

libepoxy (1.5.2-0.2) unstable; urgency=medium

  * Non-maintainer upload
  * libepoxy-dev Depends on libgl1-mesa-dev and libegl1-mesa-dev.
    Those packages provide gl.pc and egl.pc, which are depended on by
    epoxy.pc since 1.5.2 (Closes: #909173)
  * Work around #874077 by creating an Xvfb screen where GLX can work

 -- Simon McVittie <smcv@debian.org>  Wed, 19 Sep 2018 10:39:51 +0100

libepoxy (1.5.2-0.1) unstable; urgency=medium

  * Non-maintainer upload
  * New upstream version 1.5.2 (Closes: #906951)
  * Add minimal debian/gbp.conf
  * debian/copyright: temporarily exclude .gitignore
  * Update Vcs links for migration to https://salsa.debian.org
  * Bump debhelper compat to 11
  * debian/libepoxy0.symbols: Update
  * Build-Depend on libgl1-mesa-dev
  * Build with meson
  * Run build tests and make tests failures fail the build on Ubuntu
  * Cherry-pick dispatch-Fix-GLES3-symbol-lookup.patch

 -- Jeremy Bicha <jbicha@debian.org>  Tue, 18 Sep 2018 15:09:26 -0400

libepoxy (1.4.3-1) unstable; urgency=medium

  * New upstream version 1.4.3 (Closes: #881540).
  * Section: debian-installer for udeb
  * Copyright: factor Expat license
  * Move maintenance to collab-maint, use gbp layout

 -- Jérémy Lal <kapouer@melix.org>  Mon, 13 Nov 2017 12:44:13 +0100

libepoxy (1.3.1-3) unstable; urgency=medium

  * Drop patch causing amdgpu-pro lock (Closes: #850033)
  * Add add_null_checks.patch (upstreamed, avoids some segfaults).

 -- Jérémy Lal <kapouer@melix.org>  Sat, 24 Jun 2017 10:07:39 +0200

libepoxy (1.3.1-2) unstable; urgency=medium

  * Set myself as maintainer, with previous maintainer authorization.
  * Standards-Version 3.9.8
  * B-D python:native for cross-build. Closes: #842711.
    Thanks to Helmut Grohne <helmut@subdivi.de>
  * Build libepoxy0-udeb so libgtk-3-0-udeb can depend on it.
    Closes: #788711. Thanks to Cyril Brulebois <kibi@debian.org>.

 -- Jérémy Lal <kapouer@melix.org>  Fri, 20 Jan 2017 09:40:39 +0100

libepoxy (1.3.1-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Backport three upstream commits. (Closes: #850033)

 -- Jérémy Lal <kapouer@melix.org>  Thu, 19 Jan 2017 01:19:17 +0100

libepoxy (1.3.1-1) unstable; urgency=low

  * Upgrade to upstream v1.3.1 release and make the -dev package multi-arch.
    (Closes: #787314)

 -- Eric Anholt <eric@anholt.net>  Wed, 15 Jul 2015 16:36:27 -0700

libepoxy (1.2-1) unstable; urgency=low

  * Upgrade to upstream v1.2 release.

 -- Eric Anholt <eric@anholt.net>  Tue, 13 May 2014 17:47:29 -0700

libepoxy (1.1-1) unstable; urgency=low

  * Initial release (Closes: #737273)

 -- Eric Anholt <eric@anholt.net>  Fri, 31 Jan 2014 16:52:20 -0800
